import React, { useContext } from 'react';
import { HomeIcon, ShoppingBagIcon } from '@heroicons/react/24/solid';
import Link from 'next/link';
import { CartContext } from '../../context/cartContext';

const Header = () => {
  const { cart } = useContext(CartContext);

  return (
    <div className="px-5 max-w-screen-xl mx-auto shadow-sm ">
      <div className="flex items-center gap-10 justify-center py-6">
        <Link href="/">
          <a>
            <HomeIcon className="w-8 h-8 text-gray-500 hover:text-gray-600 transition-colors " />
          </a>
        </Link>
        <h1 className="text-3xl text-center font-bold ">plnts-assignment</h1>
        <Link href="/cart">
          <a className="relative">
            <ShoppingBagIcon className=" w-8 h-8 text-gray-500 hover:text-gray-600 transition-colors" />
            {cart.length > 0 && (
              <p className="bg-yellow-100 text-xs text-yellow-700 w-4 h-4 rounded-full flex items-center justify-center absolute bottom-0 right-0">
                {cart.length}
              </p>
            )}
          </a>
        </Link>
      </div>
    </div>
  );
};

export default Header;
