import { gql, GraphQLClient } from 'graphql-request';

const graphQlAPI = process.env.NEXT_PUBLIC_GRAPHCMS_ENDPOINT;

export default async function addToCart(req, res) {
  const graphQLClient = new GraphQLClient(graphQlAPI, {
    headers: {
      authorization: `Bearer ${process.env.GRAPHCMS_TOKEN}`,
    },
  });

  const query = gql`
    mutation MyMutation($products: ID!, $quantity: Int!) {
      createCart(
        data: { quantity: $quantity, products: { connect: { id: $products } } }
      ) {
        id
      }
    }
  `;

  try {
    const result = await graphQLClient.request(query, req.body);
    return res.status(200).send(result);
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
}
