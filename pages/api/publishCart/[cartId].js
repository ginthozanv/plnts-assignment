import { gql, GraphQLClient } from 'graphql-request';
const graphQlAPI = process.env.NEXT_PUBLIC_GRAPHCMS_ENDPOINT;

export default async function publishCart(req, res) {
  const { cartId } = req.query;

  const graphQLClient = new GraphQLClient(graphQlAPI, {
    headers: {
      authorization: `Bearer ${process.env.GRAPHCMS_TOKEN}`,
    },
  });

  const query = gql`
    mutation publishCart($cartId: ID!) {
      publishCart(where: { id: $cartId }, to: PUBLISHED) {
        id
      }
    }
  `;

  try {
    const result = await graphQLClient.request(query, { cartId });
    return res.status(200).send(result);
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
}
