import React, { useContext, useEffect } from 'react';
import Header from '../../components/Header';
import { CartContext } from '../../context/cartContext';

const Cart = () => {
  const { cart, deleteCartItem, getCardItems } = useContext(CartContext);

  useEffect(() => {
    getCardItems();
  }, []);

  return (
    <div className="max-w-screen-xl mx-auto ">
      <Header />
      <div className="my-10">
        <div className="overflow-hidden border">
          <table className="min-w-full divide-y divide-gray-300">
            <thead className="">
              <tr>
                <th
                  scope="col"
                  className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6"
                >
                  Product
                </th>
                <th
                  scope="col"
                  className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                >
                  Quantity
                </th>
                <th
                  scope="col"
                  className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                >
                  Price
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody className="divide-y divide-gray-200 bg-white">
              {cart.map((item) => (
                <tr key={item.id}>
                  <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm sm:pl-6">
                    <div className="flex items-center">
                      <div className="h-20 w-20 flex-shrink-0">
                        <img
                          className="h-20 w-20 border p-2"
                          src={
                            item.products[0].images[0]?.productImages[0]
                              ?.images[0]?.url
                          }
                          alt=""
                        />
                      </div>
                      <div className="ml-4">
                        <div className="font-medium text-gray-900">
                          {item.products[0].name}
                        </div>
                      </div>
                    </div>
                  </td>
                  <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                    {item.quantity}
                  </td>
                  <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                    {item.products[0].price}
                  </td>
                  <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                    <button onClick={() => deleteCartItem(item.id)}>
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Cart;
