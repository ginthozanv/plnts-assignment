import { createContext, useEffect, useState } from 'react';
import {
  addToCartApi,
  deleteCartItemApi,
  getCarts,
  publishCartApi,
} from '../services';

export const CartContext = createContext(null);

export const CartContextProvider = ({ children }) => {
  const [cart, setCart] = useState([]);

  const addToCart = async (product) => {
    try {
      const { createCart } = await addToCartApi(product);
      const res = await publishCartApi(createCart.id);
      if (res) {
        getCardItems();
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  const getCardItems = async () => {
    try {
      const carts = (await getCarts()) || [];
      setCart(carts || []);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteCartItem = async (id) => {
    try {
      await deleteCartItemApi(id);
      getCardItems();
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCardItems();
  }, []);

  const value = { cart, addToCart, getCardItems, deleteCartItem };

  return <CartContext.Provider value={value}>{children}</CartContext.Provider>;
};
