import { gql, GraphQLClient } from 'graphql-request';

const graphqlAPI = process.env.NEXT_PUBLIC_GRAPHCMS_ENDPOINT;
const hygraph = new GraphQLClient(graphqlAPI);

export const getProducts = async () => {
  const query = gql`
    query MyQuery {
      products {
        id
        name
        price
        slug
        images {
          productImages {
            images {
              url
            }
          }
        }
      }
    }
  `;

  const result = await hygraph.request(query);
  return result.products;
};

export const getProductDetails = async (id) => {
  const query = gql`
    query MyQuery($id: ID!) {
      product(where: { id: $id }) {
        id
        categories {
          name
        }
        description
        name
        price
        images {
          productImages {
            images {
              url
            }
          }
        }
      }
    }
  `;

  const result = await hygraph.request(query, { id });
  return result.product;
};

export const addToCartApi = async (obj) => {
  const results = await fetch('/api/addToCart', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(obj),
  });

  return results.json();
};

export const getCarts = async () => {
  const query = gql`
    query GetCarts {
      carts {
        quantity
        id
        products {
          id
          name
          price
          images {
            productImages {
              images {
                url
              }
            }
          }
        }
      }
    }
  `;

  const result = await hygraph.request(query);
  return result.carts;
};

export const publishCartApi = async (id) => {
  const results = await fetch(`/api/publishCart/${id}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  });

  return results.json();
};

export const deleteCartItemApi = async (id) => {
  const results = await fetch(`/api/deleteCart/${id}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  });

  return results.json();
};
